<?php

// untuk menyertakan file yang dibutuhkan, apabila file yang disertakan tidak ditemukan akan ada fatal error.
require('animal.php');
require('ape.php');
require('frog.php');

echo "<h3>Tugas 9 - OOP PHP</h3>";
// inheritance shaun    
$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded <br>"; // "no"

// index.php
// objek baru kelas frog
$kodok = new Frog("buduk");

echo "<br>Name : $kodok->name <br>"; // "buduk"
echo "legs : $kodok->legs <br>"; // 4
echo "cold blooded : $kodok->cold_blooded <br>"; // "no"
$kodok->jump() ; // "hop hop"

//object baru kelas ape
$sungokong = new Ape("kera sakti");

echo "<br> Name : $sungokong->name <br>"; // "kera sakti"
echo "legs : $sungokong->legs <br>"; // 2
echo "cold blooded : $sungokong->cold_blooded <br>"; // "no"
$sungokong->yell(); // "Auooo"

?>